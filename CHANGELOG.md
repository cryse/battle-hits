# Changelog

## v1.7.2

* fix vehicle assamble on first load

## v1.7.1

* fix camera forced update
* fix component matrix bug
* validate preview vehicle with simplified compactDescr
* vehicle present validator (help wooden PC)

## v1.7.0

* fix crash (undisposed elements cause crash randomly)
* fix hit validation

## v1.6.9

* fix camera force update intervals
* use ingame vehicle assambler

## v1.6.8

* fix steelhunter

## v1.6.7

* fix forced camera position
* fix errors in log on mod UI close
* fix crashes on GameObject no longer available
* fix UI fonts

## v1.6.6

* repo moved to gitlab