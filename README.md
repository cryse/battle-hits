﻿
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=wot-public-mods_battle-hits&metric=alert_status)](https://sonarcloud.io/dashboard?id=wot-public-mods_battle-hits) 
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=wot-public-mods_battle-hits&metric=ncloc)](https://sonarcloud.io/dashboard?id=wot-public-mods_battle-hits)
[![Visits](https://gitlab.poliroid.ru/api/badge/battle-hits/visits)](https://gitlab.com/wot-public-mods/battle-hits)
[![Downloads](https://gitlab.poliroid.ru/api/badge/battle-hits/downloads)](https://gitlab.com/wot-public-mods/battle-hits/-/releases)
[![Donate](https://cdn.poliroid.ru/gitlab/donate.svg)](https://poliroid.ru/donate)

**BattleHits** This is a modification for the game "World Of Tanks" which allows you to show shots dealt by you or to your vehicle in lobby

### An example of what a mod looks
![An example of what a mod looks](https://static.poliroid.ru/battleHits.jpg)
